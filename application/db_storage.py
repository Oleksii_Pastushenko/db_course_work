import psycopg2
import sqlalchemy
import pandas as pd


def read_sql():
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    sql = "select * from product"
    df = pd.read_sql(sql, connection)
    connection.close()
    return df


def write_csv_product_to_db():
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    df = pd.read_csv('./../data/product.csv', sep=',', encoding='latin1')
    sql_schema = "insert into product (product_name, product_category, sold, price) values ('{}','{}',{},{})"
    i = 0
    cursor = connection.cursor()
    sql = ""
    for i in range(len(df.index)):
        sql = sql + sql_schema.format('{' + df.iloc[i]['product_name'] + '}',
                                      '{' + df.iloc[i]['product_category']+'}', df.iloc[i]['sold'],
                                      df.iloc[i]['price']) + ';' + '\n'
    cursor.execute(sql)
    cursor.close()
    connection.commit()
    connection.close()


def write_history():
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    df_max = pd.read_sql("select max(id) from product", connection)
    i = df_max['max'].iloc[0]
    df = pd.read_csv('./../data/history.csv', sep=',', encoding='latin1')
    sql_schema = "insert into history (product_id, sold, price) values ({},{},{})"
    h = 0
    cursor = connection.cursor()
    sql = ""
    for h in range(len(df.index)):
        pr_id = df['product_id'].iloc[h] + i
        sql = sql + sql_schema.format(pr_id,
                                      df.iloc[h]['sold'], df.iloc[h]['price']) + ';' + '\n'
    cursor.execute(sql)
    cursor.close()
    connection.commit()
    connection.close()


def write_history_sql():
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    df = pd.read_csv('./../data/history_from_sql.csv', sep=',', encoding='latin1')
    sql_schema = "insert into history (product_id, sold, price) values ({},{},{})"
    cursor = connection.cursor()
    sql = ""
    for h in range(len(df.index)):
        sql = sql + sql_schema.format(df['product_id'].iloc[h],
                                      df.iloc[h]['sold'], df.iloc[h]['price']) + ';' + '\n'
    cursor.execute(sql)
    cursor.close()
    connection.commit()
    connection.close()


def df_for_cat_plot():
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    sql = "select h.product_category as pd,sum(sold*price) as pr from categories h " \
          "inner join (select * from product) m on m.product_category = h.product_category \n group by pd"
    df = pd.read_sql(sql, connection, index_col='pd')
    return df


def df_for_prod():
    sql = "with k as(select k.id, sum(k.sold + m.sold) as sold, avg(k.price + m.price) " \
          "as price from product k inner join (select product_id, sold, price from history) m on k.id=m.product_id" \
          " group by k.id)," \
          " m as(select id,sold,price from product" \
          " where id not in (select id from k))," \
          " f as(select * from k union (select * from m)" \
          " order by id)," \
          " cmoney as(" \
          " select id,(sold*price) as got from f)" \
          " select id,got from cmoney"
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    df = pd.read_sql(sql, connection, index_col='id')
    return df


def trend_price(id_p):
    sql_scheme = "with m as(select id,price,sold from product union " \
                 "(select product_id as id,price,sold from history order by id) ) " \
                 "select sold, price from m " \
                 "where id = {}"
    sql = sql_scheme.format(id_p)
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    df = pd.read_sql(sql, connection, index_col='sold', columns=['price'])
    return df


def overall_sells():
    sql = "with k as(select k.id, sum(k.sold + m.sold) as sold, sum(k.price + m.price) " \
          "as price from product k inner join (select product_id, sold, price from history) m on k.id=m.product_id " \
          "group by k.id), " \
          "m as(select id,sold,price from product " \
          "where id not in (select id from k)), " \
          "res as(select id,sold as sol, price as prc from k union select * from m), " \
          "hm as (select *  from product t inner join (select * from res) jo on jo.id = t.id " \
          "order by t.id) " \
          "select h.product_category as pc, sum(sold)*avg(price) as pr from categories h" \
          " inner join (select * from hm) jk on jk.product_category = h.product_category " \
          "group by pc"
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    df = pd.read_sql(sql, connection)
    return df
