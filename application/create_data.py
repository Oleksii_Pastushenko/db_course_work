import pandas as pd
import random as rnd
import db_storage as dbs


def create_product_csv():
    fixed_df = pd.read_csv('./../data/categories.csv', sep=',', encoding='latin1')
    categories = fixed_df['Category']
    fixed_df = pd.read_csv('./../data/items.csv', sep=',', encoding='latin1')
    items = fixed_df['Items']
    df1 = pd.DataFrame(columns=['id', 'product_name', 'product_category', 'sold', 'price'])
    i = 1
    for i in range(1001):
        df2 = pd.DataFrame({'id': [i],
                            'product_name': [rnd.choice(items)],
                            'product_category': [rnd.choice(categories)],
                            'sold': [rnd.randint(10, 100)],
                            'price': [round(rnd.triangular(10, 1000), 2)]})
        df1 = df1.append(df2)
    df1.to_csv("./../data/product.csv", index=False)
    return df1


def create_history_csv():
    fixed_df = pd.read_csv('./../data/product.csv', sep=',', encoding='latin1')
    ids = fixed_df['id']
    df1 = pd.DataFrame(columns=['product_id', 'sold', 'price'])
    i = 1
    for i in range(1001):
        df2 = pd.DataFrame({'product_id': [rnd.choice(ids)],
                            'sold': [rnd.randint(10, 100)],
                            'price': [round(rnd.triangular(10, 1000), 2)]})
        df1 = df1.append(df2)
    df1.to_csv("./../data/history.csv", index=False)
    return df1


def create_history_from_db():
    df = dbs.read_sql()
    ids = df['id']
    df1 = pd.DataFrame(columns=['product_id', 'sold', 'price'])
    i = 1
    for i in range(1001):
        df2 = pd.DataFrame({'product_id': [rnd.choice(ids)],
                            'sold': [rnd.randint(10, 100)],
                            'price': [round(rnd.triangular(10, 1000), 2)]})
        df1 = df1.append(df2)
    df1.to_csv("./../data/history_from_sql.csv", index=False)
    return df1

