import create_data as cd
import pyautogui
import db_storage as dbs
import os
import plot as plt
import subprocess
import array
import psycopg2


def main_menu():
    while True:
        pyautogui.hotkey('command', 'l')
        print("MAIN MENU")
        print("1 - DB menu")
        print("2 - create data menu")
        print("3 - backup menu")
        print("4 - plot menu")
        print("5 - exit")
        k = eval(input("Enter key ..."))
        if k == 1:
            pyautogui.hotkey('command', 'l')
            db_menu()
        elif k == 2:
            pyautogui.hotkey('command', 'l')
            create_data_menu()
        elif k == 3:
            pyautogui.hotkey('command', 'l')
            backup_menu()
        elif k == 4:
            pyautogui.hotkey('command', 'l')
            plot_menu()
        elif k == 5:
            break


def db_menu():
    while True:
        print("DB MENU")
        print("1 - see db")
        print("2 - write products to db from csv")
        print("3 - write history for csv to db from csv")
        print("4 - write history for sql to db from csv")
        print("5 - exit")
        k = eval(input("Enter key ..."))
        pyautogui.hotkey('command', 'l')
        if k == 1:
            db = dbs.read_sql()
            print(db)
        elif k == 2:
            dbs.write_csv_product_to_db()
        elif k == 3:
            dbs.write_history()
        elif k == 4:
            dbs.write_history_sql()
        elif k == 5:
            break


def create_data_menu():
    while True:
        print("CREATE DATA MENU")
        print("1 - create product csv")
        print("2 - create history csv")
        print("3 - create history csv for db")
        print("4 - exit")
        k = eval(input("Enter key ..."))
        pyautogui.hotkey('command', 'l')
        if k == 1:
            df1 = cd.create_product_csv()
            print(df1)
        elif k == 2:
            df1 = cd.create_history_csv()
            print(df1)
        elif k == 3:
            df1 = cd.create_history_from_db()
            print(df1)
        elif k == 4:
            break


def backup_menu():
    while True:
        print("BACKUP MENU")
        print("1 - make backup")
        print("2 - load backup")
        print("3 - exit")
        k = eval(input("Enter key ..."))
        pyautogui.hotkey('command', 'l')
        if k == 1:
            command = r'"C:\Program Files\PostgreSQL\12\bin\pg_dump" ' \
                      r'--dbname=postgresql://postgres:1111@127.0.0.1:5432/postgres -c > C:\dump\dump4.sql'
            os.system(command)
        elif k == 2:
            command = r'"C:\Program Files\PostgreSQL\12\bin\psql" ' \
                      r'--dbname=postgresql://postgres:1111@127.0.0.1:5432/postgres < C:\dump\dump4.txt'
            os.system(command)
        elif k == 3:
            break


def plot_menu():
    while True:
        print("PLOT MENU")
        print("1 - make sells by category without history")
        print("2 - overall money by product")
        print("3 - trend for product")
        print("4 - make sells by category with history")
        print("5 - exit")
        k = eval(input("Enter key ..."))
        pyautogui.hotkey('command', 'l')
        if k == 1:
            plt.plot1()
        elif k == 2:
            plt.plot2()
        elif k == 3:
            print("Enter product id")
            item = eval(input("Enter item id ..."))
            plt.plot3(item)
        elif k == 4:
            plt.plot4()
        elif k == 5:
            break

