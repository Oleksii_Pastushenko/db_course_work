import pandas as pd
import db_storage as dbs
import matplotlib.pyplot as plt


def plot1():
    db = dbs.df_for_cat_plot()
    print(db)
    plt.style.use('ggplot')
    plt.rcParams['figure.figsize'] = (20, 15)
    fig = db.plot(kind='bar',  figsize=(30, 20), fontsize=26).get_figure()
    fig.savefig('./../data/plots/plot1.pdf')


def plot2():
    db = dbs.df_for_prod()
    print(db)
    plt.style.use('ggplot')
    plt.rcParams['figure.figsize'] = (20, 15)
    fig = db.plot(kind='bar', figsize=(30, 20), fontsize=26).get_figure()
    fig.savefig('./../data/plots/plot2.pdf')


def plot3(id_p):
    db = dbs.trend_price(id_p)
    plt.style.use('ggplot')
    plt.rcParams['figure.figsize'] = (20, 15)
    print(db)
    db = db.cumsum()
    fig = db.plot(figsize=(30, 20), fontsize=26).get_figure()
    fig.savefig('./../data/plots/plot3.pdf')


def plot4():
    db = dbs.df_for_cat_plot()
    print(db)
    plt.style.use('ggplot')
    plt.rcParams['figure.figsize'] = (20, 15)
    fig = db.plot(kind='bar', figsize=(30, 20), fontsize=26).get_figure()
    fig.savefig('./../data/plots/plot4.pdf')
